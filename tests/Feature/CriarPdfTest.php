<?php

namespace Tests\Feature;

use Adapter\RelatorioAdapterPdf;
use Legado\Estoque;
use PHPUnit\Framework\TestCase;

class CriarPdfTest extends TestCase
{
    public function testSucesso()
    {
        $relatorioAdapter = new RelatorioAdapterPdf();
        $estoque = new Estoque($relatorioAdapter);
        $estoque->gerarPdf();

        $this->assertFileExists('./exemplos/tmp/estoque.pdf');
    }
}