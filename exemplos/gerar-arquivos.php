<?php

require 'vendor/autoload.php';

$relatorioAdapter = new Adapter\RelatorioAdapterPdf();
$estoque = new \Legado\Estoque($relatorioAdapter);
$estoque->gerarPdf();

echo 'OK' . PHP_EOL;