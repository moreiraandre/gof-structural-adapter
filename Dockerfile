FROM php:8.1

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    composer

RUN apt-get update && \
    apt-get install -y git vim zip libpng-dev && \
    docker-php-ext-install gd && \
    pecl install xdebug && \
    docker-php-ext-enable xdebug

RUN echo xdebug.mode=coverage > /usr/local/etc/php/conf.d/xdebug.ini

RUN mkdir /estudo

ENV PUID 1000
ENV PGID 1000
ENV NEW_USER estudo

RUN groupadd -g ${PGID} ${NEW_USER} && \
    useradd -l -u ${PUID} -g ${NEW_USER} -m ${NEW_USER} && \
    usermod -p "*" ${NEW_USER} -s /bin/bash

USER ${NEW_USER}

WORKDIR /estudo
