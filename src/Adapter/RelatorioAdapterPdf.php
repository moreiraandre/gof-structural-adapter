<?php

namespace Adapter;

use Relatorios\GeradorPdf;

class RelatorioAdapterPdf implements RelatorioAdapterInterface
{
    private GeradorPdf $geradorPdf;

    public function __construct()
    {
        $this->geradorPdf = new GeradorPdf();
    }

    public function gerar(string $dadosXml): void
    {
        $dadosJson = $this->xmlParaHtml($dadosXml);
        $this->geradorPdf->gerar($dadosJson);
    }

    private function xmlParaHtml(string $dadosXml): string
    {
        $xml = simplexml_load_string($dadosXml);
        $xml = json_encode($xml);
        $listaProdutos = json_decode($xml, true);
        $listaProdutos = $listaProdutos['item'];

        $listaProdutosHtml = '<h1 align="center">Relatório de Estoque</h1>';
        $listaProdutosHtml .= '<table border="1" width="100%">';
        $listaProdutosHtml .= '<tr>';
        $listaProdutosHtml .= '<th>Nome</th>';
        $listaProdutosHtml .= '<th>Qtde.</th>';
        $listaProdutosHtml .= '<th>Valor</th>';
        $listaProdutosHtml .= '</tr>';

        foreach ($listaProdutos as $produto) {
            $listaProdutosHtml .= '<tr>';
            $listaProdutosHtml .= '<td>' . $produto['nome'] . '</td>';
            $listaProdutosHtml .= '<td>' . $produto['quantidade'] . '</td>';
            $listaProdutosHtml .= '<td>' . $produto['valorUnitario'] . '</td>';
            $listaProdutosHtml .= '</tr>';
        }
        $listaProdutosHtml .= '</table>';

        return $listaProdutosHtml;
    }
}