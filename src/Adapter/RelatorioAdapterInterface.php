<?php

namespace Adapter;

interface RelatorioAdapterInterface
{
    public function gerar(string $dadosXml): void;
}