<?php

namespace Legado;

use Adapter\RelatorioAdapterInterface;
use Faker\Factory as FakerFactory;
use Faker\Generator as GeneratorFaker;

class Estoque
{
    private GeneratorFaker $faker;

    public function __construct(private readonly RelatorioAdapterInterface $relatorioAdapter)
    {
        $this->faker = FakerFactory::create();
    }

    public function gerarPdf()
    {
        $listaProdutos = $this->listaProdutos();
        $this->relatorioAdapter->gerar($listaProdutos);
    }

    public function listaProdutos(): string
    {
        $produtos = '';
        for ($quantidadeProdutos = 0; $quantidadeProdutos < 10; $quantidadeProdutos++) {
            $produto = '<item>';
            $produto .= '<nome>' . $this->faker->words(2, true) . '</nome>';
            $produto .= '<quantidade>' . rand(10, 20) . '</quantidade>';
            $produto .= '<valorUnitario>' . $this->faker->randomFloat(2, 10, 50) . '</valorUnitario>';
            $produto .= '</item>';
            $produtos .= $produto;
        }

        return '<estoque>' . $produtos . '</estoque>';
    }
}