<?php

namespace Relatorios;

class GeradorPdf
{
    public function gerar(string $dadosHtml): void
    {
        $mpdf = new \Mpdf\Mpdf(['tempDir' => './']);
        $mpdf->WriteHTML($dadosHtml);
        $mpdf->Output('./exemplos/tmp/estoque.pdf', 'F');
    }
}